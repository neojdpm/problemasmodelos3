import random
import math
from probability import *
import statistics
from ProblemaCola import neg_exp

def procesamiento_B(X):
	if X>=1 and X<=3:
		return 0.25*(X-1)
	elif X>3 and X<=5:
		return 0.25*(5-X)

def main():
	# Frecuencia_Trabajos = math.floor(neg_exp(5/60))
	A = 0	# Trabajos en A
	ocupado_A = False	# A está ocupado
	B = 0	# Trabajos en B
	ocupado_B = False	# B está ocupado
	t = 0	# En minutos
	N = 10000
	procesamiento_acumulado_A = 0	# Para determinar si A puede trabajar o no
	procesamiento_acumulado_B = 0	# Para determinar si A puede trabajar o no
	trabajos_totales = 0	# Número total de trabajos recibidos
	trabajos_procesados = 0	# Número total de trabajos realizados
	tiempo_espera_A = 0

	print(30%60)

	while t<N:
		if t%60 == 0:	# Llegada de los trabajos por hora
			A += math.floor(neg_exp(5/60))
			trabajos_totales += A
		if A!=0:
			if not ocupado_A:
				Procesamiento_A = random.uniform(6,10)
				procesamiento_acumulado_A += Procesamiento_A
				procesamiento_acumulado_B += Procesamiento_A
				ocupado_A = True
				if t==0:
					procesamiento_acumulado_B = Procesamiento_A
			else:
				if procesamiento_acumulado_A<t:
					if B<=4:
						A -= 1
						B += 1
						ocupado_A = False
					else:
						tiempo_espera_A += 1
		if B!=0:
			if not ocupado_B:
				proc_B = procesamiento_B(B)
				procesamiento_acumulado_B += proc_B
				ocupado_B = True
			else:
				if procesamiento_acumulado_B<t:
					B -= 1
					ocupado_B = False
					trabajos_procesados += 1
		t += 1

	return([trabajos_totales/N,tiempo_espera_A/t,procesamiento_acumulado_B/trabajos_procesados])
	#print('El número esperado de trabajos en el taller en cualquier momento (minuto) es de {0:0.0f}'.format(trabajos_totales/N))
	#print('El porcentaje de tiempo que el centro A está deternido por esperar al centro B es del {0:0.2f}%'.format(tiempo_espera_A/t))
	# Ojo: Esto es porque 0.25*(x-1) = 0 cuando x==1. Por ende, el centro B siempre procesa instantáneamente apenas le llega algo!
	#print('El tiempo esperado de terminación de un trabajo es de {0} minutos'.format(procesamiento_acumulado_B/trabajos_procesados))

N = 1000
muestras = []
for test in range(0, N):
	muestras.append(main())

print("Número de trabajos esperados promedio: {} con intervalo de confianza: {}".format(statistics.median(muestras[0]),confianza(muestras[0], statistics.median(muestras[0]), N, 0.1)))
print("Porcentaje de tiempo que se para el centro A: {} con intervalo de confianza: {}".format(statistics.median(muestras[1]),confianza(muestras[1], statistics.median(muestras[1]), N, 0.1)))
print("Tiempo esperado de terminación de un trabajo: {} con intervalo de confianza: {}".format(statistics.median(muestras[2]),confianza(muestras[2], statistics.median(muestras[2]), N, 0.1)))