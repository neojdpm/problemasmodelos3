import random
import probability
import statistics

# class RandomClass:

#     def init(self, names, probs):
#         self.names = names
#         self.probs = probs

#     def getRandom(self):
#         rand = random.random()
#         index = 0
#         cumulative = 0
#         for prob in self.probs:
#             cumulative += prob
#             if (cumulative >= rand): return self.names[index]
#             index += 1

def neg_exp(lamb):
    return random.expovariate(lamb)

class Cliente:
    def __init__(self,tiempo_llegada,tiempo_inicio_servicio,tiempo_servicio):
        self.tiempo_llegada = tiempo_llegada
        self.tiempo_inicio_servicio = tiempo_inicio_servicio
        self.tiempo_servicio = tiempo_servicio
        self.tiempo_fin_servicio = self.tiempo_inicio_servicio+self.tiempo_servicio
        self.espera = self.tiempo_inicio_servicio-self.tiempo_llegada

class Cajero:
    tiempo_inicio_servicio = 0
    def __init__(self,tiempo_inicio_servicio,tiempo_servicio,ocupado,tiempo_en_servicio):
        self.tiempo_inicio_servicio = tiempo_servicio
        self.tiempo_servicio = tiempo_servicio
        self.ocupado = ocupado
        self.tiempo_en_servicio = tiempo_en_servicio+self.tiempo_servicio
    def tiempo_ocioso(self,tiempo_simulacion):
        tiempo_ocioso = tiempo_simulacion-self.tiempo_en_servicio

# Inicialización

def problemacola(): 
    t= 0            # Tiempo de inicio
    Clientes = []   # Vector con los clientes
    Cajeros = []    # Vector con los cajeros
    lamb = 1        # Media de tiempo de llegada: 1 min por usuario
    tiempo_simulacion = 420 # Número de iteraciones de la simulación (7 horas al día que está abierto el banco)
    # mts = 4          # Media del tiempo de servicio
    pd = 0          # Probabilidad de declinar

    for i in range(0,4):
        Cajeros.append(Cajero(0,0,0,0))

    # Simulación

    while (t<tiempo_simulacion):
        cajeros_libres = [cl for cl in Cajeros if cl.ocupado==0]
        current = random.randint(0,len(cajeros_libres)-1)
        if len(Clientes)==0:
            tiempo_llegada = neg_exp(lamb)
            # tiempo_inicio_servicio = tiempo_llegada
            Cajeros[current].tiempo_inicio_servicio = tiempo_llegada
        else:
            tiempo_llegada += neg_exp(lamb)
            Cajeros[current].tiempo_inicio_servicio = max(tiempo_llegada, Clientes[-1].tiempo_fin_servicio)
        if Cajeros[current].ocupado==0:
            Cajeros[current].tiempo_servicio = random.uniform(3,5)
            Cajeros[current].ocupado==1
            if len(Clientes)<=5:
                pd = 0
            elif (len(Clientes)>=6 and len(Clientes)<=8):
                pd = 2
            elif (len(Clientes)>=9 and len(Clientes)<=10):
                pd = 4
            elif (len(Clientes)>=11 and len(Clientes)<= 14):
                pd = 6
            else:
                pd = 8
            cd = random.uniform(0,9) # El cliente declina
            if pd<cd:
                Clientes.append(Cliente(tiempo_llegada,Cajeros[current].tiempo_inicio_servicio,Cajeros[current].tiempo_servicio))
                Cajeros[current].tiempo_en_servicio+=Cajeros[current].tiempo_servicio
            if Clientes[-1].tiempo_fin_servicio==(Cajeros[current].tiempo_servicio+t):
                Cajeros[current].ocupado=0
        t = tiempo_llegada

    # Cálculo de las estadísticas

    #print("Pregunta 1:")

    tiempos_espera = [c.espera for c in Clientes]
    espera_media = sum(tiempos_espera)/len(tiempos_espera)
    #print("El tiempo medio de espera es de {0:0.1f} minutos por cliente".format(espera_media))
    #print("Se quedaron {0} clientes de un total de {1} clientes, lo cual signfica que declinaron el {2:0.2f}% de los clientes."\.format(len(Clientes),tiempo_simulacion,(len(Clientes)/tiempo_simulacion)*100))
    tiempos_ociosos = [c.tiempo_en_servicio for c in Cajeros]
    
    #print("El porcentaje de tiempo desocupado promedio de los cajeros es del {0}%.".format([to/tiempo_simulacion*100 for to in tiempos_ociosos]))

    #print("Pregunta 2:\nTomando en cuenta que la probabilidad de declinar de los clientes disminuye, se atienden más clientes\por período. Por ende, el desempeño del sistema aumenta.")

    return [espera_media, (len(Clientes)/tiempo_simulacion)*100, [to/tiempo_simulacion*100 for to in tiempos_ociosos]]

    #s = [0,0,0,0]   # Servidores (cajeros)
    #CO = 0          # Clientes Observados 
    #q = 0           # Número de clientes en cola
    #TE = 0          # Tiempo de espera
    #A = []          # Vector con tiempos de llegada entre los clientes
    #pd = 0          # Probabilidad de declinar
    #ml = 1          # Media de tiempo de llegada de 1 minuto por usuario
    #ms = 4          # Media de tiempo de servicio
    #n = 420         # Número de iteraciones de la simulación (7 horas al día que está abierto el banco)

    #tserv = random.uniform(3,5) # Ojo, agregar en cada iteración. La media es 4

    #for iteracion in range(1,n): # Las iteraciones ocurren cada minuto
    #    if (q>=0 and q<=5):
    #        pd = 0
    #    elif (q>=6 and q<=8):
    #        pd = 0.2
    #    elif (q>=9 and q<=10):
    #        pd = 0.4
    #    elif (q>=11 and q<= 14):
    #        pd = 0.6
    #    else:
    #        pd =0.8

    #    q = q + 1*(1-pd)


    # boats = RandomClass(["Tanque", "Mediano", "Pequeno"], [0.4,0.35,0.25])
    # haces boats.getRandom() y te da uno de los nombres en la primera lista
    # segun las probabilidades respectivas

N = 1000
resultado8 = {}
muestras = [[],[],[],[],[],[],[]]
for k in range(0, N):
    iteracion = problemacola()
    muestras[0].append(iteracion[0])
    muestras[1].append(iteracion[1])
    muestras[2].append(iteracion[2][0])
    muestras[3].append(iteracion[2][1])
    muestras[4].append(iteracion[2][2])
    muestras[5].append(iteracion[2][3])

print(muestras)
media0 = sum(muestras[0])/N
media1 = sum(muestras[1])/N

media2 = sum(muestras[2])/N
media3 = sum(muestras[3])/N
media4 = sum(muestras[4])/N
media5 = sum(muestras[5])/N
print("TIEMPO")
probability.confianza(muestras[0], media0, 1000, 0.10)
print("DECLINA")
probability.confianza(muestras[1], media1, 1000, 0.10)

print("CAJEROS")
probability.confianza(muestras[2], media2, 1000, 0.10)
probability.confianza(muestras[3], media3, 1000, 0.10)
probability.confianza(muestras[4], media4, 1000, 0.10)
probability.confianza(muestras[5], media5, 1000, 0.10)