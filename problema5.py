
from probability import RandomClass
from collections import deque
import statistics

days = 50

class Terminal:
    actual = None
    diaslibres = 0
    trabajo = 0
    
    def __init__(self, tiempos):
        self.tiempos = tiempos #tiempo de trabajo en terminal tanque, mediano, peque;o

    def ocupar(self, bote):
        if bote == "Tanque": tiempo = self.tiempos[0]
        elif bote ==  "Mediano": tiempo = self.tiempos[1]
        elif bote ==  "Pequeno": tiempo = self.tiempos[2]
        self.actual = bote
        self.trabajo = tiempo
        
    def dia(self):
        if (self.trabajo):
            self.trabajo -= 1
        else:
            self.actual = None
            self.diaslibres += 1

    def desocupado(self): 
        return self.trabajo == 0





def problema5():
    time = 0 # Days
    boats = RandomClass(["Tanque", "Mediano", "Pequeno"], [0.4,0.35,0.25])
    llegadas = RandomClass([1,2,3,4,5], [0.2,0.25,0.35, 0.15, 0.05])
    terminalA = Terminal([4,3,2])
    terminalB = Terminal([3,2,1])
    puerto = deque()

    nextBoat = None
    nextDay = -1
    while (time < days):
        # Set llegadas
        print("Day: " + str(time))
        if (nextDay == time):
            print("Arrived: " + nextBoat)
            puerto.append(nextBoat)

        if (nextDay <= time):
            nextDay = llegadas.getRandom() + time
            nextBoat = boats.getRandom()
            print("Next: {0} in {1} days".format(nextBoat, str(nextDay - time)))
        
        if len(puerto):
            if terminalB.desocupado():
                terminalB.ocupar(puerto.popleft())
            elif terminalA.desocupado():
                terminalA.ocupar(puerto.popleft())
        
        time += 1
        terminalA.dia()
        terminalB.dia()

    return {
        "puerto": len(puerto),
        "terminalA": terminalA.diaslibres,
        "terminalB": terminalB.diaslibres
    }

print(problema5())