from problema5 import problema5
from problema7 import problema7
from problema8 import problema8

N = 100

def sum_dicts(d1, d2):
    for k, v in d2.items():
        try:
            d1[k] += v
        except:
            d1[k] = v
    return d1
    
def median_dict(d1, N):
    for k, v in d1.items():
        d1[k] /= N
    return d1


resultado5 = {
    "puerto": 0,
    "terminalA": 0,
    "terminalB": 0
}
resultado8 = {}
problema7()
for k in range(0, N):
    #sum_dicts(resultado8, problema8())
    """
    iteracion = problema5()
    resultado5["puerto"] += iteracion["puerto"]
    resultado5["terminalA"] += iteracion["terminalA"]
    resultado5["terminalB"] += iteracion["terminalB"]
    """

print("Promedio de venta de vendedores:")
print(median_dict(resultado8,N))

print("Promedio: longitud del puerto, dias libres A y dias libres B")
print(resultado5["puerto"]/N)
print(resultado5["terminalA"]/N)
print(resultado5["terminalB"]/N)