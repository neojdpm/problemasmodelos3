
from probability import *
from collections import deque

def problema8():
    autos = RandomClass(["Compacto", "Mediano", "Lujo"], [0.4,0.35,0.25])
    vendidos = RandomClass([0,1,2,3,4,5], [0.10, 0.15, 0.20, 0.25, 0.20, 0.10])
    comisionCompacto = 250
    comisionMediano = RandomClass([400,500], [0.40, 0.60])
    comisionLujo = RandomClass([1000,1500,2000], [0.35, 0.40, 0.25])

    comisiones = {}
    for vendedor in range(0,5):
        autosVendidos = vendidos.getRandom()
        comision = 0
        for i in range(0, autosVendidos):
            auto = autos.getRandom()
            if (auto == "Compacto"):
                comision += 250
            elif (auto == "Mediano"):
                comision += comisionMediano.getRandom()
            elif (auto == "Lujo"):
                comision += comisionLujo.getRandom()

        comisiones[vendedor] = comision

    return comisiones

N = 100
resultado8 = {}
muestras = []
for k in range(0, N):
    iteracion = problema8()
    muestras.append(iteracion[0])
    sum_dicts(resultado8, iteracion)
    
medias = median_dict(resultado8, N)
confianza(muestras, medias[0], 100, 0.10)

