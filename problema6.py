
tabla = [127, 162, 179, 75, 223, 186, 124, 45, 100, 171, 235, 176, 130, 159, 117, 100, 92, 68, 242, 122, 184, 84, 240, 319, 61, 78, 20, 141, 202, 213, 204, 360, 169, 206, 326, 210, 335, 233, 102, 243, 135, 310, 138, 95, 216, 99, 346, 220, 191, 230, 219, 225, 271, 270, 110, 305, 157, 128, 163, 90, 148, 70, 40, 80, 105, 159, 141, 150, 164, 200, 213, 195, 134, 141, 107, 177, 109, 48, 145, 114, 400, 212, 258, 198, 229, 175, 199, 177, 194, 185, 303, 335, 310, 104, 374, 190, 211, 160, 138, 227, 122, 230, 97, 166, 232, 187, 212, 125, 119, 90, 286, 310, 115, 277, 189, 159, 266, 170, 28, 141, 155, 309, 152, 122, 262, 111, 254, 124, 138, 190, 136, 110, 396, 96, 86, 111, 81, 226, 50, 134, 131, 120, 112, 140, 280, 145, 208, 333, 250, 221, 318, 120, 72, 166, 194, 87, 94, 170, 65, 190, 359, 312, 205, 77, 197, 359, 174, 140, 167, 181, 143, 99, 297, 92, 246, 211, 275, 224, 171, 290, 291, 220, 239, 126, 89, 66, 35, 26, 129, 234, 181, 180, 58, 40, 54, 123, 78, 319, 389, 121 ]

'''
Simulacion del problema 6 de la guia
'''
from probability import *
from random import random, expovariate, uniform
from math import log, sqrt
from scipy import stats
from collections import deque
import statistics


media = 176
sd = 83 # Desviacion estandar de la distribucion que sigue el numero
# de pasajeros que se embarcan en una estacion

class Tren:
    
    tiempo = 0
    pasajeros_embarcados = []
    cantidad_embarcados_por_estacion = []

class Pasajero:
    tiempo_en_el_tren = 0

def generar_pasajeros_que_embarca():
    pasajeros = []
    muestra = expovariate(1/media)
    muestra = int(round(muestra, 0))
    for i in range(muestra):
        pasajeros.append(Pasajero())
    
    return pasajeros


def calculo_tiempo_recorrido(num_pasajeros):
    '''
    Calcula el tiempo de recorrido entre
    una estacion y otra
    '''
    if num_pasajeros > 0:
        return 100*(1 + 0.1*(log(num_pasajeros, 10)))
    return 0

def calculo_tiempo_embarque_desembarque(num_desembarque, num_embarque):
    '''
    Calcula el tiempo de embarque y desembarque de los pasajeros
    '''
    if num_desembarque + num_embarque > 0:
        return 20*(1 + 0.1*(log(num_desembarque + num_embarque, 10)))
    return 0

def pasajero_se_baja_del_tren(estacion):
    '''
    Genera el numero de estaciones que recorrera el
    pasajero en el tren
    '''
    num_aleatorio = random()
    if num_aleatorio < 0.5:
        return True
    return False

def simulacion():
    '''
    Funcion que simula el problema
    '''
    tren = Tren() # Se crea el tren

    numero_pasajeros_por_parada = []
    estacion_actual = 1 # Estacion en la cual se encuentra actualmente el tren

    while estacion_actual <= 10:
        
        numero_pasajeros_por_parada.append(len(tren.pasajeros_embarcados))
        tren.tiempo += calculo_tiempo_recorrido(len(tren.pasajeros_embarcados)) # Tiempo que dura en llegar a la estacion
        pasajeros_desembarcan = []

        # Veo que pasajeros se desembarcan
        for pasajero in tren.pasajeros_embarcados:
            pasajero_se_baja = pasajero_se_baja_del_tren(estacion_actual)
            if pasajero_se_baja:
                tren.pasajeros_embarcados.remove(pasajero) # Desmonto al pasejero
                pasajeros_desembarcan.append(pasajero) # Lo saco del tren

        pasajeros_embarca = generar_pasajeros_que_embarca() # Genera los pasajeros que se van a embarcar
        tren.pasajeros_embarcados += pasajeros_embarca # Monto a los pasajeros
        tren.cantidad_embarcados_por_estacion.append(len(pasajeros_embarca))
        tren.tiempo += calculo_tiempo_embarque_desembarque(len(pasajeros_desembarcan), len(pasajeros_embarca)) # Calculo el tiempo en la estacion

        estacion_actual += 1
    
    # Se termina la simulacion
    return tren, numero_pasajeros_por_parada, numero_pasajeros_por_parada

def print_intervalo(promedio, delta):
    return "[" + str(round(promedio - delta,2)) + ", " + str(round(promedio + delta,2)) + "]"

tren_resultado, pasajeros_por_parada, pasajeros_en_el_tren = simulacion()



def main(iteraciones):
    tiempo_por_iteracion = []
    media_pasajeros_array = []
    promedio_de_pasajeros_en_el_tren = []
    maximo_de_pasajeros_embarcados = []

    iter = 0
    while iter < iteraciones:
        aux1, aux2, aux3 = simulacion()
        tiempo_por_iteracion.append(aux1.tiempo)
        aux2 = sum(pasajeros for pasajeros in aux2) / 10
        media_pasajeros_array.append(aux2)
        aux3 = sum(num_pasajeros for num_pasajeros in aux3) / 10
        promedio_de_pasajeros_en_el_tren.append(aux3)
        maximo_de_pasajeros_embarcados.append(max(aux1.cantidad_embarcados_por_estacion))
        iter += 1

    tiempo_promedio = sum(tiempo for tiempo in tiempo_por_iteracion)/iteraciones

    # RESPUESTAS A LAS PREGUNTAS
    # a) Tiempo total recorrido
    print("Tiempo total recorrido por el tren fue: " + str(tiempo_promedio))
    confianza(tiempo_por_iteracion, statistics.median(tiempo_por_iteracion), iteraciones, 0.1)

    # b) El numero de pasajeros promedio a bordo del tren.
    promedio_pasajeros = sum(pasajeros for pasajeros in promedio_de_pasajeros_en_el_tren)/iteraciones
    print("Numero de pasajeros promedio a bordo del tren: " + str(promedio_pasajeros))
    confianza(promedio_de_pasajeros_en_el_tren, statistics.median(promedio_de_pasajeros_en_el_tren), iteraciones, 0.1)


    # c) El numero maximo de pasajeros embarcados
    promedio_maximo = sum(maximo for maximo in maximo_de_pasajeros_embarcados) / iteraciones
    print("Numero maximo de pasajeros embarcados: " + str(round(promedio_maximo,0)))
    confianza(maximo_de_pasajeros_embarcados, statistics.median(maximo_de_pasajeros_embarcados), iteraciones, 0.1)
    


main(100)