import random
import math
import scipy.stats as st

class RandomClass:

    def __init__(self, names, probs):
        self.names = names
        self.probs = probs

    def getRandom(self):
        rand = random.random()
        index = 0
        cumulative = 0
        for prob in self.probs:
            cumulative += prob
            if (cumulative >= rand): return self.names[index]
            index += 1
        
def sum_dicts(d1, d2):
    for k, v in d2.items():
        try:
            d1[k] += v
        except:
            d1[k] = v
    return d1
    
def median_dict(d1, N):
    for k, v in d1.items():
        d1[k] /= N
    return d1


def confianza(Xi, median, N, alpha=0.1):
    sumstd = 0
    for x in Xi:
        sumstd += (x - median)**2
    std = math.sqrt(sumstd / (N - 1))
    z = st.norm.ppf(1 - alpha/2)
    delta = z*std/math.sqrt(N)
    print("Intervalo de confianza para el valor, alpha={0}%:".format(alpha*100))
    print("[{0}, {1}]".format(median - delta, median + delta))
    return [median - delta, median + delta]

