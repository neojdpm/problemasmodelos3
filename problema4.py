from scipy import stats
import random
import math

def neg_exp(lamb):
    return random.expovariate(lamb)

def exp(lamb=1):
    return 1 -  math.exp(- lamb* random.random())

def problema4():
	n = 4	# Número de máquinas trabajando
	s = 3	# Número de máquinas disponibles como repuesto
	d = 0	# Número de máquinas defectuosas
	F = exp(1)	# Función de distribución para el tiempo de descomposición
	G = exp(2)	# Función de distribución para el tiempo de reparación
	t = 0	# Tiempo total

	fda_f = stats.expon.cdf(1) # Función de distribución acumulada para F
	fda_g = stats.expon.cdf(2) # Función de distribución acumulada para G
	print(F)
	print(G)

	while d!=7:
		pd = random.random() # Probabilidad de descomposición
		pr = random.random() # Probabilidad de reponer
		if n>0:
			if pd<F:
				n-=1
				d+=1
				if s>0:
					s-=1
					n+=1
		if d>0:
			if pr<G:
				d-=1
				s+=1
		t+=1
	print("El tiempo de falla del sistema fue de fue de {} UT".format(t))
	return(t)
print(exp(1))
exp(1)
problema4()
#N = 100
#resultado4 = {}
#muestras = []
#for k in range(0, N):
#    iteracion = problema4()
#    muestras.append(iteracion)
    
#media = sum(resultado4) / N
#confianza(muestras, medias, N, 0.10)