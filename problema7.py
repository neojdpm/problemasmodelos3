
from probability import *
from collections import deque
import statistics

def problema7(
    Q = 100,
    inv = 100,
    R=20, 
    days = 30
):

    dailydemand = RandomClass([12,13,14,15,16,17], [0.05, 0.15, 0.25, 0.35, 0.15, 0.05])
    delivery = RandomClass([1,2,3,4], [0.20, 0.30, 0.35, 0.15])

    coste = 0
    escasez = 0
    nextDay = -1
    escasez = 0
    requested = False

    for time in range(0,days):
        if (requested and (nextDay == time)):
            inv += Q
            requested = False

        # Suplir demandas retrasadas
        if (escasez):
            if (inv < escasez):
                escasez -= inv
                inv = 0
            else:
                inv -= escasez
                escasez = 0

        demand = dailydemand.getRandom()

        if (inv < demand):
            escasez += demand - inv
            inv = 0
            coste += escasez
        else: 
            inv -= demand
        coste += 0.2*inv

        if (R >= inv and not requested):
            nextDay = delivery.getRandom() + time
            requested = True
            coste += 10

    return coste

N = 50
M = 100
muestras = []
for test in range(0, N):
    minValue = 1000000
    minR = 0
    for r in range(10,25):
        coste = 0
        for k in range(0, M):
            coste += problema7(R=r)
        
        coste /= M

        if (coste < minValue):
            minValue = coste
            minR = r
    muestras.append(minR)
        
    print("Mejor Coste: {0} R: {1}".format(minValue, minR))
    
confianza(muestras, statistics.median(muestras), N, 0.1)
